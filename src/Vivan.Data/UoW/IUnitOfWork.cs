﻿using System;
using System.Threading.Tasks;

namespace Vivan.Data.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> CommitAsync();
    }
}
