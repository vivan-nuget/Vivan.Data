﻿using System.Data;

namespace Vivan.Data.Factories
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
