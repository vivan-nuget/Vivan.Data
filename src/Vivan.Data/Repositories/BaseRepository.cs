﻿using System;
using Vivan.Data.Factories;

namespace Vivan.Data.Repositories
{
    public abstract class BaseRepository : IDisposable
    {
        protected readonly IDbConnectionFactory DbConnectionFactory;

        protected BaseRepository(IDbConnectionFactory dbConnectionFactory)
        {
            DbConnectionFactory = dbConnectionFactory;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
